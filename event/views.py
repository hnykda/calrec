from django.shortcuts import render, HttpResponse, render_to_response, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from .models import Event
from messg.models import Messg
from .forms import EventForm
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt

def develop(random_sample_size, top_n, modulator, date_of_event, _title, subject):
    from .call_recoms import get_data_for_event
    jsonedmails = get_data_for_event(random_sample_size, top_n, modulator)
    ev = Event(date_and_time=date_of_event, title=_title)
    ev.save()

    for recipient, body in jsonedmails.items():
        Messg.objects.create(body=body, recipient=recipient, subject=subject, event=ev)

    return ev

@csrf_exempt
def show_calendar(request):
    if request.method == "POST":
        if ("strategyLoyalty" in request.POST) or ("strategyMargin" in request.POST):
            day, month, year = list(map(int, request.session.pop("dayCreate").split("/")))
            if "strategyLoyalty" in request.POST:
                modulator = 1
            elif "strategyMargin" in request.POST:
                modulator = 1.5
            ev = develop(100, 20, modulator,datetime(year=year, month=month, day=day) , 
                    "Recommendations" + str(day+month) , "Recommendations for you")
            return HttpResponseRedirect('/event/{}'.format(ev.id))
        request.session["dayCreate"] = request.POST.get("dayToCreate")
        return render(request, 'strategy.html')

    events = Event.objects.all()
    now = datetime.now()
    return render(request, 'cal_template.html', 
            {"year": now.year,"month":now.month, 
             "next_month":now.month+1, "events":events, "next_next_month":now.month+2 })

def show_event(request, event_id):
    event = Event.objects.get(id=event_id)
    d = {"event" : event}

    if request.method == "POST":
        if "sentMails" in request.POST:
#        print(request.POST.get('sentMails'))
            event.send_emails()
            event.sent = True
            event.save()
            return render(request, 'sent.html', d)
    return render(request, 'show_event.html', d)


def create_event(request):
    if request.method == "POST":
        form = EventForm(request.POST)
        if form.is_valid():
            modulator = form.cleaned_data["modulator"]
            random_sample_size = form.cleaned_data["random_sample_size"]
            top_n = form.cleaned_data["top_n"]
            date_of_event = form.cleaned_data["date_of_event"]
            _title = form.cleaned_data["title"]
            subject = form.cleaned_data["subject"]

            ev = develop(random_sample_size, top_n, modulator, date_of_event, _title, subject)

            return redirect('/event/{}'.format(ev.id))
    else:
        form = EventForm()
    return render(request, 'create_event.html', {'form' : form })




@csrf_exempt
def multip(request):
    if request.method == "POST":
#        print(dir(request.POST.values))
        dates = request.POST.get("dates")
        request.session["dates"] = dates
        return strategy_choose(request, dates)
    return render(request, "multi.html")

@csrf_exempt
def strategy_choose(request, dates):
    if ("strategyLoyalty" in request.POST) or ("strategyMargin" in request.POST):
        if "strategyLoyalty" in request.POST:
            modulator = 1
        elif "strategyMargin" in request.POST:
            modulator = 1.5
        request.session["modulator"] = modulator 
        return choose_schema(request, dates, modulator)
    return render(request, 'strategy.html')

@csrf_exempt
def choose_schema(request, dates, modulator):
    print("ha")
    print(dir(request.POST.values))
    if "schema" in request.POST:
        print("he")
        schm = request.POST.get("schema")
        return finalize(request, dates, modulator, schm)
    return render(request, 'schema.html')

def develop2(date_of_event, _title, subject, product, recps):
    from .call_recoms import get_data_for_event
    ev = Event(date_and_time=date_of_event, title=_title, product=product)
    ev.save()

    for recipient in recps:
        Messg.objects.create(body="None", recipient=recipient, subject=subject, event=ev)
    return ev

def finalize(request, dates, modulator, schm):
    # choose most promising products
    # redistribute them over days
    # create events for given products
    # show dashboard
    from post_precomputation import load_those_we_have_recs_for
    ps = load_those_we_have_recs_for()

    e = [(p, p.price*float(modulator)) for p in ps]
    s = sorted(e, key=lambda x:x[1], reverse=True)[:int(schm)]
    
    from recs_for_prod import get_recipients    
    recips = get_recipients([x[0] for x in s])

    days = []
    for date in dates.split(","):
        year, month, day = date.split("/")
        days.append(datetime(year=year, month=month, day=day))
    
    for prod, recip in recips.items():
        i = len(days) 
        r = len(recip) // i
        y = 0
        for day in days:
            if y+r <= len(recip):
                ev = develop2(day, "Recommendations" + str(day+month) , "Recommendations for you", product, recip[y:y+r])
            y += r
    
    return redirect("/calendar/")           
