from django.db import models
from prods.models import Product
import logging
log = logging.getLogger(__name__)

class Event(models.Model):
    date_and_time = models.DateTimeField(blank=False)
    title = models.CharField(default="Send recommendations", max_length=30)
    sent = models.BooleanField(default=False, verbose_name="Has been already sent?")
    product = models.ForeignKey(Product, blank=True, default=None)

    def get_absolute_url(self):
        return "/event/%i" % self.id

    def send_emails(self):
        for mail in self.messg_set.all():
            try:
                mail.send()
            except Exception as ex:
                log.error("Mail to {} has not been sent because of {}".format(mail.recipient, str(ex)))

