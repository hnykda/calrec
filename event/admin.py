from django.contrib import admin

from .models import Event

class EventAdmin(admin.ModelAdmin):
    list_display = ("title", "date_and_time", "sent")

admin.site.register(Event, EventAdmin)
