from django.contrib.auth.models import User
from prods.models import Product
from random import sample
from recommends.providers import recommendation_registry
import logging

l = logging.getLogger(__name__)
P = recommendation_registry.get_provider_for_content(Product).storage 

def get_recommendations_for_n_user(n): 
    res = {}
    l.debug("Getting recommendations for {} users".format(n))
    allusers = sample(list(User.objects.all()), n)
    mxu = len(allusers)
    for ix,usr in enumerate(allusers): 
        recs = P.get_recommendations_for_user(usr)
        if len(recs) > 0:
            l.warn("Processing {}/{}".format(ix, mxu))
            res[usr] = recs
    l.debug("We have {} recommendations".format(len(recs)))
    return res

def get_n_most_potential_users(res, n, modulator):
    """ 
    modulator is how much we should count with price
    1: no change
    <1: price has more importance
    >1: price has less importance 
    """
    import statistics
    
    most_pote = {}
    for user, recs in res.items():
        most_pote[user] = (statistics.mean(
                [i.score*(i.object.price * modulator) for i in recs]), recs)
    max_users = sorted(most_pote.items(), key=lambda x: x[1][0], reverse=True)[:n]
    return dict((user, recs[1]) for user, recs in max_users )

def format_message(user, recs):
    s = "Dear user {0},\nthese products might be interesting for you:\n\n".format(user.username)
    for prod in recs:
        obj = prod.object
        s += "* {0} for ${1} (code: {2})\n".format(obj.name, obj.price, obj.prodID)

    s += "\nSincerely,\nyour seller."
    return s

def get_emails(res):
    msgs = {}
    for us, recs in res.items():
        msgs[us.email] = format_message(us, recs)
    return msgs

def get_data_for_event(n_users, top_n, modulator):
    res = get_recommendations_for_n_user(n_users)
    top_users = get_n_most_potential_users(res, top_n, modulator)
    jsoned = get_emails(top_users)
    return jsoned
