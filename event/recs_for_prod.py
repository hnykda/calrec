from recommends.storages.djangoorm.models import Recommendation
from django.contrib.auth.models import User

RECS = Recommendation.objects.all()

def get_mails_with_product_recom(product, limit):
    size = Recommendation.objects.count()
    if limit > size or limit == None:
        limit = size
    flt = filter(lambda i: i.object.id == product.id, RECS[:limit])
    addrs = [User.objects.get(id=x.user).email for x in flt]
    return addrs

def get_recipients(products, limit=None):
    # products: iterable of products to look for

    recips_for_product = {}
    for product in products:
        recips_for_product[product.id] = get_mails_with_product_recom(product, limit)

    return recips_for_product
