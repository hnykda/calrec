# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-10 19:29
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='date_and_time',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='event',
            name='formated_emails',
            field=jsonfield.fields.JSONField(default={'useremail1@example.com': 'product id 3'}),
        ),
    ]
