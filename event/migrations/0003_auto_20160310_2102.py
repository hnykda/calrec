# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-10 21:02
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0002_auto_20160310_1929'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='sent',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='event',
            name='formated_emails',
            field=jsonfield.fields.JSONField(),
        ),
    ]
