from django import forms
import datetime

class EventForm(forms.Form):
    modulator = forms.FloatField(min_value=1, label="Modulator (> 1)",help_text="How much is a price of a product important", initial=1.2)
    #random_sample_size = forms.IntegerField(label="How big random sample of users/products should be (recommended: 200)")
    random_sample_size = forms.IntegerField(
            label="Random Sample Size", 
            help_text="For how many random users we should look for recommendations", initial=200)
    top_n = forms.IntegerField(label="Number of emails", help_text="How many most perspective customers you want to sent an emai", initial=100)
    date_of_event = forms.DateTimeField(label="Date of event", initial=datetime.date.today, help_text="Date when mails should be send" )
    title = forms.CharField(max_length=30, label="Title of event", help_text="How the event should be called", initial="Recommendations")
    subject = forms.CharField(max_length=30, label="Subject", help_text="Subject of emails", initial="Recommendations for you!")
