from django.contrib import admin

from .models import Messg

class MsgAdmin(admin.ModelAdmin):
    list_display = ("subject", "recipient")

admin.site.register(Messg, MsgAdmin)
# Register your models here.
