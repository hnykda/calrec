from django.db import models
from event.models import Event
from logging import getLogger
log = getLogger(__name__)

class Messg(models.Model):
    body = models.TextField(blank=False, verbose_name="Body of email")
    subject = models.CharField(max_length=40, verbose_name="Subject") 
    recipient = models.EmailField(blank=False, verbose_name="Recipient")
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    sent = models.BooleanField(default=False, verbose_name = "Was sent?")

    def send(self):
        log.error("NOT IMPLEMENTED!")
        log.info("Sending email to {} (object_id = {})".format(self.recipient, self.id))
        log.debug("Mail sent to {}".format(self.recipient))
        self.sent = True
        self.save()
