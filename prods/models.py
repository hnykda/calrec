from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

class Product(models.Model):
    """A generic Product"""
    prodID = models.CharField(max_length=10, unique=True, blank=False)
    name = models.CharField(blank=True,max_length=100)
    sites = models.ManyToManyField(Site)
    price = models.FloatField(blank=False)

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('product_detail', [self.id])

    def sites_str(self):
        return ', '.join([s.name for s in self.sites.all()])
    sites_str.short_description = 'sites'


class PRank(models.Model):
    """A Rank of a Product for a user"""
    user = models.ForeignKey(User, related_name='pranks')
    product = models.ForeignKey(Product)
    site = models.ForeignKey(Site)
    score = models.FloatField(blank=False)

    def __str__(self):
        return "PRank"
