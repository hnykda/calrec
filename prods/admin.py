from django.contrib import admin

from .models import Product, PRank

class ProductAdmin(admin.ModelAdmin):
    list_display = ("prodID", "name", "price" )

class PRankAdmin(admin.ModelAdmin):
    list_display = ("user", "product", "score")

admin.site.register(Product, ProductAdmin)
admin.site.register(PRank, PRankAdmin)
