from django.contrib.auth.models import User
from recommends.providers import RecommendationProvider
from recommends.providers import recommendation_registry
from random import sample
from .models import Product, PRank
from djrec.settings import SAMPLE_SIZE

import logging
l =logging.getLogger(__name__)


class ProductRecommendationProvider(RecommendationProvider):
    def get_users(self):
        l.info("Sampling users by {}".format(SAMPLE_SIZE))
        return sample(list(User.objects.filter(is_active=True, pranks__isnull=False).distinct()), SAMPLE_SIZE)

    def get_items(self):
        l.info("Sampling products by {}".format(SAMPLE_SIZE))
        return sample(list(Product.objects.all()), SAMPLE_SIZE)

    def get_ratings(self, obj):
        return PRank.objects.filter(product=obj)

    def get_rating_score(self, rating):
        return rating.score

    def get_rating_site(self, rating):
        return rating.site

    def get_rating_user(self, rating):
        return rating.user

    def get_rating_item(self, rating):
        return rating.product

recommendation_registry.register(PRank, [Product], ProductRecommendationProvider)
